//
//  BaseVC.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import UIKit

class BaseVC: UIViewController {

    //MARK: - UIVIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - POP THE VIEW CONTROLLER
    public func POP(_ animated : Bool = true){
        self.navigationController?.popViewController(animated: animated)
    }
    
    //MARK: - DISMISS THE VIEW CONTROLLER
    public func DISMISS(_ animated : Bool = true){
        self.dismiss(animated: animated, completion: nil)
    }
    
    //MARK: - SHOW THE ALERT
    public func SHOW_ALERT(_ title:String = .kEmpty,_ message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: .kOK, style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - PUSH THE MOVIE DETAILS VIEW CONTROLLER
    public func PUSH_MOVIE_DETAILS_SCREEN(urlString:String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier:IdentifierVC.kMovieDetailsVC) as! MovieDetailsVC
        vc.urlString = urlString
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
