//
//  MovieListTextField.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation
import UIKit

//===========================================
//MARK: - UITextFieldDelegate
//===========================================
extension ViewController :UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.movieCollectionView.performBatchUpdates {
                if updatedText == "" {
                    self.filterMovie.removeAll()
                    self.isFilter = false
                }
                else{
                    self.isFilter = true
                    self.filterMovie = self.viewModelArray.filter { $0.title!.lowercased().contains(updatedText.lowercased()) }
                }
            } completion: { bool in
                self.movieCollectionView.reloadData()
            }
        }
        return true
    }
}
