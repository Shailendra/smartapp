//
//  AyncImageView.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 12/03/22.
//

import Foundation
import UIKit

//MARK: - STOR CASH IMAGE ARRAY
var asyncImagesCashArray = NSCache<NSString, UIImage>()


//MARK: - AYNC IMAGE VIEW CALSS
class AyncImageView: UIImageView {
    
    //MARK: - VARIABLE FOR CURRENT URL
    private var currentURL: NSString?
    
    //MARK: - LOAD ASYNCE IMAGE FROM URL
    func loadAsyncFrom(url: String, placeholder: UIImage?) {
        let imageURL = url as NSString
        if let cashedImage = asyncImagesCashArray.object(forKey: imageURL) {
            image = cashedImage
            return
        }
        image = placeholder
        currentURL = imageURL
        guard let requestURL = URL(string: url) else { image = placeholder; return }
        URLSession.shared.dataTask(with: requestURL) { (data, response, error) in
            DispatchQueue.main.async { [weak self] in
                if error == nil {
                    if let imageData = data {
                        if self?.currentURL == imageURL {
                            if let imageToPresent = UIImage(data: imageData) {
                                asyncImagesCashArray.setObject(imageToPresent, forKey: imageURL)
                                self?.image = imageToPresent
                            } else {
                                self?.image = placeholder //MARK: - RETURN PLACEHOLDER
                            }
                        }
                    } else {
                        self?.image = placeholder //MARK: - RETURN PLACEHOLDER
                    }
                } else {
                    self?.image = placeholder //MARK: - RETURN PLACEHOLDER
                }
            }
        }.resume()
    }
}
