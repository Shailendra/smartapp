//
//  MovieListCollectionView.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation
import UIKit

//=============================================================
//MARK: - UICollectionViewDataSource, UICollectionViewDelegate
//=============================================================
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.isFilter ? self.filterMovie.count : self.viewModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PopularCell.identifier, for: indexPath as IndexPath) as! PopularCell
        cell.deleteMovieButton.tag = indexPath.item
        cell.deleteMovieButton.addTarget(self, action: #selector(ViewController.deleteMovieAction(sender:)), for: .touchUpInside)
        let vote_average = self.isFilter ? self.filterMovie[indexPath.item].vote_average! :  self.viewModelArray[indexPath.item].vote_average!
        if vote_average > 7 {
            cell.backdropView.isHidden = false
            cell.posterView.isHidden = true
            cell.urlString = self.isFilter ? self.filterMovie[indexPath.item].backdrop_path! :  self.viewModelArray[indexPath.item].backdrop_path!
        }
        else{
            cell.backdropView.isHidden = true
            cell.posterView.isHidden = false
            cell.urlString = self.isFilter ? self.filterMovie[indexPath.item].poster_path! : self.viewModelArray[indexPath.item].poster_path!
            cell.movieTitleLabel.text = self.isFilter ? self.filterMovie[indexPath.item].title! : self.viewModelArray[indexPath.item].title ?? .kEmpty
            cell.movieOverViewLabel.text = self.isFilter ? self.filterMovie[indexPath.item].overview! : self.viewModelArray[indexPath.item].overview ?? .kEmpty
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.PassImageDetailViewController(indexPath: indexPath.item)
    }
}

