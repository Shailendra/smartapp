//
//  ViewController.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import UIKit

class ViewController: BaseVC {

    //MARK: - PROPERTIES
    @IBOutlet weak var movieCollectionView : UICollectionView!
    @IBOutlet weak var searchTextField     : UITextField!
    
    var viewModel      = MovieListVM()
    var viewModelArray = [Results]()
    var filterMovie    = [Results]()
    var isFilter = false
   
    //MARK: - UIVIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initCollectionView()
        self.viewModel.callWebAppOfMovieList { isSuccess,popular in
            if isSuccess{
                self.viewModelArray   = popular
                self.movieCollectionView.reloadData()
            }
        }
    }
    
    //MARK: - REGISTER THE COLLECTIONVIEW CELL
    public func initCollectionView(){
        self.movieCollectionView.register(PopularCell.nib, forCellWithReuseIdentifier: PopularCell.identifier)
    }
}

