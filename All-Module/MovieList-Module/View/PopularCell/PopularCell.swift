//
//  PopularCell.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import UIKit

class PopularCell: UICollectionViewCell {

    //MARK: - PROPERTIES
    var placehoder = UIImage(named: ImageName.kPlaceholder.rawValue)
    @IBOutlet weak var backdropImageView  : AyncImageView!
    @IBOutlet weak var posterImageView    : AyncImageView!
    @IBOutlet weak var movieTitleLabel    : UILabel!
    @IBOutlet weak var deleteMovieButton  : UIButton!
    @IBOutlet weak var movieOverViewLabel : UILabel!
    @IBOutlet weak var backdropView       : UIView!
    @IBOutlet weak var posterView         : UIView!
    
    public static var identifier : String { return String(describing: self)}
    public static var nib : UINib { return UINib(nibName: identifier, bundle: nil) }
    
    var urlString: String? {
        didSet {
            if let url = urlString {
                self.backdropImageView.loadAsyncFrom(url: ImageBaseURL + url, placeholder:placehoder)
                self.posterImageView.loadAsyncFrom(url: ImageBaseURL + url, placeholder:placehoder)
            }
        }
    }
    
    //MARK: - AWAKE FORM NIB
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setCornerRadius()
    }
    
    //MARK: - SET CONNER RADIUS
    public func setCornerRadius(){
        self.backdropImageView.layer.cornerRadius = 10
        self.posterImageView.layer.cornerRadius   = 5
        self.deleteMovieButton.layer.cornerRadius = 5
    }
}
