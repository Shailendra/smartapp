//
//  MovieListFunction.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation
import UIKit

//==================================
//MARK: -EXTENSION VIEWCONTROLLER
//==================================
extension ViewController{
    
    //MARK: - DELETE MOVIE ACTION
    @objc func deleteMovieAction(sender: UIButton){
        let alert = UIAlertController(title: Message.kDeleteMovie, message: Message.kAreYouSsure, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Message.kNO, style: .default, handler: { action in
        }))
        alert.addAction(UIAlertAction(title: Message.kYES, style: .default, handler: { action in
            self.movieCollectionView.performBatchUpdates {
                self.viewModelArray.remove(at: sender.tag)
            } completion: { bool in
                self.movieCollectionView.reloadData()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - PASS THE IMAGE IN DETAIL VIEW CONTROLLER
    public func PassImageDetailViewController(indexPath: Int){
        let vote_average = self.isFilter ? self.filterMovie[indexPath].vote_average! :  self.viewModelArray[indexPath].vote_average!
        if vote_average > 7 {
            self.PUSH_MOVIE_DETAILS_SCREEN(urlString:self.isFilter ? self.filterMovie[indexPath].backdrop_path! :  self.viewModelArray[indexPath].backdrop_path!)
        }
        else{
            self.PUSH_MOVIE_DETAILS_SCREEN(urlString: self.isFilter ? self.filterMovie[indexPath].poster_path! :  self.viewModelArray[indexPath].poster_path!)
        }
    }
}

