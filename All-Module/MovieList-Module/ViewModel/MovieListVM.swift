//
//  MovieListVM.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation
import UIKit


class MovieListVM : NSObject{
    //MARK: - MARK:- PROPERTIES
    var popularfilterArray = [Results]()
}

extension MovieListVM {

    //MARK: - CALL WEB SERVICE OF MOVIES LIST
    public func callWebAppOfMovieList(handler:@escaping (_ isSuccess:Bool,_ popular:[Results]) -> Void){
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        WebService.shared.request(urlStr: END_POINT.NOW_PLAYING.rawValue, httpMethod: .GET, params: nil, callback: { (result, error) in
            DispatchQueue.main.async {
                do {
                    //MARK: - RESPONSE OF WEB SERVICE AND DECODE THE DATA
                    let jsonDecoder = JSONDecoder()
                    let upMovie = try jsonDecoder.decode(PopularCodable.self, from: result!)
                    self.popularfilterArray = upMovie.results ?? []
                }
                catch {
                }
                dispatchGroup.leave()
            }
        })
        dispatchGroup.notify(queue: .main) { [self] in
            handler(true,popularfilterArray)
        }
    }
}
