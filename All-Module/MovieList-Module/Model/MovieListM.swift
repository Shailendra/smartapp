//
//  MovieListM.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation

//MARK: - POPULAR CODEABLE
struct PopularCodable : Codable {
    let dates         : Dates?
    let page          : Int?
    let results       : [Results]?
    let total_pages   : Int?
    let total_results : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case dates         = "dates"
        case page          = "page"
        case results       = "results"
        case total_pages   = "total_pages"
        case total_results = "total_results"
    }
    
    init(from decoder: Decoder) throws {
        let values         = try decoder.container(keyedBy: CodingKeys.self)
        self.dates         = try values.decodeIfPresent(Dates.self, forKey: .dates)
        self.page          = try values.decodeIfPresent(Int.self, forKey: .page)          ?? .kZero
        self.results       = try values.decodeIfPresent([Results].self, forKey: .results)
        self.total_pages   = try values.decodeIfPresent(Int.self, forKey: .total_pages)   ?? .kZero
        self.total_results = try values.decodeIfPresent(Int.self, forKey: .total_results) ?? .kZero
    }
}

struct Results : Codable {
    let adult             : Bool?
    let backdrop_path     : String?
    let genre_ids         : [Int]?
    let id                : Int?
    let original_language : String?
    let original_title    : String?
    let overview          : String?
    let popularity        : Double?
    let poster_path       : String?
    let release_date      : String?
    let title             : String?
    let video             : Bool?
    let vote_average      : Double?
    let vote_count        : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case adult             = "adult"
        case backdrop_path     = "backdrop_path"
        case genre_ids         = "genre_ids"
        case id                = "id"
        case original_language = "original_language"
        case original_title    = "original_title"
        case overview          = "overview"
        case popularity        = "popularity"
        case poster_path       = "poster_path"
        case release_date      = "release_date"
        case title             = "title"
        case video             = "video"
        case vote_average      = "vote_average"
        case vote_count        = "vote_count"
    }
    
    init(from decoder: Decoder) throws {
        let values             = try decoder.container(keyedBy: CodingKeys.self)
        self.adult             = try values.decodeIfPresent(Bool.self, forKey: .adult)               ?? .kFalse
        self.backdrop_path     = try values.decodeIfPresent(String.self, forKey: .backdrop_path)     ?? .kEmpty
        self.genre_ids         = try values.decodeIfPresent([Int].self, forKey: .genre_ids)          ?? [.kZero]
        self.id                = try values.decodeIfPresent(Int.self, forKey: .id)                   ?? .kZero
        self.original_language = try values.decodeIfPresent(String.self, forKey: .original_language) ?? .kEmpty
        self.original_title    = try values.decodeIfPresent(String.self, forKey: .original_title)    ?? .kEmpty
        self.overview          = try values.decodeIfPresent(String.self, forKey: .overview)          ?? .kEmpty
        self.popularity        = try values.decodeIfPresent(Double.self, forKey: .popularity)        ?? .kZeroDouble
        self.poster_path       = try values.decodeIfPresent(String.self, forKey: .poster_path)       ?? .kEmpty
        self.release_date      = try values.decodeIfPresent(String.self, forKey: .release_date)      ?? .kEmpty
        self.title             = try values.decodeIfPresent(String.self, forKey: .title)             ?? .kEmpty
        self.video             = try values.decodeIfPresent(Bool.self, forKey: .video)               ?? .kFalse
        self.vote_average      = try values.decodeIfPresent(Double.self, forKey: .vote_average)      ?? .kZeroDouble
        self.vote_count        = try values.decodeIfPresent(Int.self, forKey: .vote_count)           ?? .kZero
    }
    
}
struct Dates : Codable {
    let maximum : String?
    let minimum : String?
    
    enum CodingKeys: String, CodingKey {
        
        case maximum = "maximum"
        case minimum = "minimum"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.maximum = try values.decodeIfPresent(String.self, forKey: .maximum) ?? .kEmpty
        self.minimum = try values.decodeIfPresent(String.self, forKey: .minimum) ?? .kEmpty
    }
}
