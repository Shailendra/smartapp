//
//  MovieDetailsVC.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import UIKit

class MovieDetailsVC: BaseVC {
    
    //MARK: - PROPERTIES
    @IBOutlet weak var moviePoster : AyncImageView!
    var urlString                  : String?
    
    //MARK: - UIVIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - LOAD THE REMAIN VIEW IN VIEW VIILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if let url = self.urlString {
            self.moviePoster.loadAsyncFrom(url: ImageBaseURL + url, placeholder:UIImage(named: ImageName.kPlaceholder.rawValue))
        }
    }
}
