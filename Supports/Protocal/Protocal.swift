//
//  Protocal.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation
import UIKit

//MARK: - PROTOCAL FOR BUTTON DELEGATE
protocol ButtonDelegate {
    func deleteMovie(sender:UIButton)
}

//MARK: - EXTENSION OF BUTTON DELEGATE
extension ButtonDelegate{
    
}
