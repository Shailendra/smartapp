//
//  Structure.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation

struct IdentifierVC{
    static let kMovieDetailsVC = "MovieDetailsVC"
}

struct Message{
    static let kDeleteMovie = "Delete Movie"
    static let kAreYouSsure = "Are you sure you want to delete this movie?"
    static let kNO          = "NO"
    static let kYES         = "YES"
}
