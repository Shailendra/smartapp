//
//  HTTP_MATHOD.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation

//MARK: - HTTP MATHOD FOR WEB SERVICE
enum HTTP_METHOD: String{
    case GET    = "GET"
    case POST   = "POST"
    case PUT    = "PUT"
    case DELETE = "DELETE"
}
