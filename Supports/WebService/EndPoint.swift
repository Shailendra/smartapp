//
//  EndPoint.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation

//MARK: - END POINT URL
enum END_POINT: String{
    case NOW_PLAYING = "now_playing?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed"
    case TRAILERS    = "209112/videos?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed"
}
