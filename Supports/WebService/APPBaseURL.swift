//
//  APPBaseURL.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation

//MARK: - APP BASE URL
let BaseURL      = "https://api.themoviedb.org/3/movie/"
let ImageBaseURL = "https://image.tmdb.org/t/p/original"
