//
//  WebService.swift
//  SmartAppsDemo
//
//  Created by Shailendra on 13/03/22.
//

import Foundation
import UIKit

//MARK: - WEBSERVICE CLASS
class WebService: NSObject {
    
    static let shared = WebService()
    let session = URLSession.shared
    
    func request(urlStr:String,httpMethod:HTTP_METHOD = .GET, params:String? = nil, callback:@escaping (_ result:Data?, _ error:Error?) -> Void) {
        
        let url = URL(string: BaseURL + urlStr)
        var request = URLRequest(url: url!)
        request.httpMethod = httpMethod.rawValue
        if params != nil{
            request.httpBody = params!.data(using: String.Encoding.utf8)
        }
        let task = session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if error == nil {
                    do {
                        callback(data!, error) //MARK: - RETURN SUCCESSFULL DATA
                    }
                    catch {
                        callback(nil, error) //MARK: - RETURN ERROR DATA
                    }
                }
                else {
                    callback(nil, error) //MARK: - RETURN ERROR DATA
                }
            }
        }
        task.resume()
    }
}
